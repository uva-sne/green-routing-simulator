"""
Given transmission rate, simulate the traffic which includes a set of flows coming in the meanwhile;
study the power consumption and delay of BCube networks
"""
import fnss
import random
import networkx as nx
import sys
import argparse
import os
import math
import csv
import numpy as np
import matplotlib.pyplot as plt

def powerCal(topology, subtopology, model, enableswbase):
    basepower = 0
    if enableswbase == 1: 
        for nodeid in subtopology.nodes(data=True):
            #print nodeid
            if nodeid[1]['type'] != 'host':
                basepower = basepower +  120
    else:
        basepower = (topology.number_of_nodes()-16)*120
    dypower = dypowerCal(subtopology, model)    
    power = basepower + dypower            
    return power

def powerCalDiff(topology, subtopology, model, enableswbase):
    basepower = 0
    seednum = 1
    nodenum = topology.number_of_nodes()
    np.random.seed(seednum)
    powerlist = np.random.normal(120,30, nodenum)
    if enableswbase == 1: 
        for nodeid in subtopology.nodes(data=True):
            #print nodeid
            if nodeid[1]['type'] != 'host':
                basepower = basepower + powerlist[nodeid[0]]
    else:
        basepower = (topology.number_of_nodes()-16)*120
    dypower = dypowerCal(subtopology, model)    
    power = basepower + dypower            
    return power

def dypowerCal(subtopology, model):
    dypower = 0
    
    if model == 'linear':
        for edge in subtopology.edges(data=True):
            wgt = float(edge[2]['weight'])
            dypower = dypower + LinearModel(wgt)
    elif model == 'step': 
        for edge in subtopology.edges(data=True):
            wgt = float(edge[2]['weight'])
            dypower = dypower + StepModel(wgt)
    elif model == 'exponent':
        for edge in subtopology.edges(data=True):
            wgt = float(edge[2]['weight'])
            dypower = dypower + ExponentModel(wgt)
    return dypower

def LinearModel(u):
    if (not u): return 0
    else: return u*0.001+1

def StepModel(u):
    if (not u): return 0
    elif u<=200:
        return 1+0.2
    elif u<=400 and u>200:
        return 1+0.4
    elif u<=600 and u>400:
        return 1+0.6
    elif u<=800 and u>600:  
        return 1+0.8
    elif u<=1000 and u>800: 
        return 1+1  
def ExponentModel(u):
    if (not u): return 0
    else:
        #print u, math.pow(u,0.6667)
        return 0.01*math.pow(u,0.6667)+1

class ShortestPath(object):
    def __init__(self, topology, tm_flow):
        self.topology = topology
        self.tm_flow = tm_flow

    def getShortestPath(self, topology, tm_flow):
        power = 0
        path_flow = tm_flow.od_pairs()[0]
        paths0 = sorted(nx.all_simple_paths(topology, path_flow[0], path_flow[1]), key=len)
        #print(paths0)
        print(paths0[0])
        path0 = paths0[0]
        len0 = len(path0)
        i=0
        delay = 0
        flow = tm_flow.flows()[(path_flow[0],path_flow[1])]
        while (i < len0-1):
            src = path0[i]
            dst = path0[i+1]
            delay = delay + topology.get_edge_data(src,dst)['delay']
            topology.add_weighted_edges_from([(src,dst,flow)])
            i=i+1
        k=1
        allpaths = path0
        while(k<len(tm_flow.od_pairs())):
            path_flow = tm_flow.od_pairs()[k]
            k=k+1
            paths2 = sorted(nx.all_simple_paths(topology, path_flow[0], path_flow[1]), key=len)
            #print(paths2)
            print(path_flow)
            flow2 = tm_flow.flows()[(path_flow[0],path_flow[1])]
            j=0
            while(j< len(paths2)):
                tmppath = paths2[j]
                i=0
                while (i < len(tmppath)-1):
                    src = tmppath[i]
                    dst = tmppath[i+1]
                    edgedata = topology.get_edge_data(src,dst)
                    used_wits = edgedata['weight'] + flow2                
                    if(used_wits>1000): 
                        j=j+1
                        break
                    i=i+1    
                if(i==len(tmppath)-1):   #path found
                    break  
            if(j==len(paths2)):##no path with enough bandwidth
                print '**********************no enought bandwidth\n'                
                tmppath = []              
            i=0
            if(tmppath):
                print 'target path:\n' 
                print(tmppath)
                while (i < len(tmppath)-1):
                        src = tmppath[i]
                        dst = tmppath[i+1]
                        edgedata = topology.get_edge_data(src,dst)
                        delay = delay + edgedata['delay']
                        used_wits = edgedata['weight'] + flow2
                        i=i+1
                        topology.add_weighted_edges_from([(src,dst,used_wits)]) 

            allpaths = tmppath + allpaths
        
        subtopology = topology.subgraph(allpaths)
        #print(subtopology.edges())

        # paths3_13 = sorted(nx.all_simple_paths(topology, 3, 12), key=len)
        # print(paths3_13)
        # print(paths3_13[0])

        # paths4_13 = sorted(nx.all_simple_paths(topology, 4, 12), key=len)
        # print(paths4_13)
        # print(paths4_13[0])
        #fnss.write_topology(topology, 'topology-wax15-flow.xml')
        fnss.write_topology(subtopology.to_directed(), 'subtopology-wax15-flow.xml')
        return subtopology, delay

#print(topology.get_edge_data(1,11))

#topo2 = topology.subgraph([1,10])
#print(topo2.edges())

#tm_flow = fnss.TrafficMatrix()
#tm_flow.add_flow(31,35,10)
#fnss.write_traffic_matrix(tm_flow, 'traffic_matrix_flow.xml')

class PriorityPath(object):
    def __init__(self, topology, tm_flow):
        self.topology = topology
        self.tm_flow = tm_flow
    def getPriorityPath(self, topology, tm_flow):
        power = 0
        path_flow = tm_flow.od_pairs()[0]
        paths0 = sorted(nx.all_simple_paths(topology, path_flow[0], path_flow[1]), key=len)
        #print(paths0)
        print(paths0[0])
        path0 = paths0[0]
        len0 = len(path0)
        i=0
        delay = 0
        flow = tm_flow.flows()[(path_flow[0],path_flow[1])]
        while (i < len0-1):
            src = path0[i]
            dst = path0[i+1]
            delay = delay + topology.get_edge_data(src,dst)['delay']
            topology.add_weighted_edges_from([(src,dst,flow)])
            i=i+1
        k=1
        allpaths = path0
        while(k<len(tm_flow.od_pairs())):
            path_flow = tm_flow.od_pairs()[k]
            flow2 = tm_flow.flows()[(path_flow[0],path_flow[1])]
            k=k+1
            paths2 = sorted(nx.all_simple_paths(topology, path_flow[0], path_flow[1]), key=len)
            #print(paths2)
            print(path_flow)
            pathlist = paths2[:]
            j = 0
            while (j< len(paths2)):
                tmppath = paths2[j]
                j=j+1
                i=0
                while (i < len(tmppath)-1):
                    src = tmppath[i]
                    dst = tmppath[i+1]
                    edgedata = topology.get_edge_data(src,dst)
                    used_wits = edgedata['weight'] + flow2                
                    if(used_wits>1000):                         
                        pathlist.remove(tmppath)
                        break
                    i=i+1    
            print("targetpathlist with enough capacity: ")
            #print(pathlist)
            if(not len(pathlist)):##if no path with enough bandwidth
                print '**********************no enought bandwidth\n'
                targetpath = []
            else:                  
                i=0
                targetlen = len(pathlist[0]) #shortest length
                targetpathlist = []
                for targetpath in pathlist:
                    if len(targetpath) > targetlen:
                        break
                    targetpathlist.append(targetpath) 
                print("targetpathlist with shortest length: ")
                print(targetpathlist)
                targetcomlen=-1       
                for tmppath in targetpathlist:
                    comnode = set(tmppath)&set(allpaths)
                    tmpcomlen = len(comnode)
                    if (tmpcomlen>targetcomlen):
                        targetcomlen=tmpcomlen
                        targetpath = tmppath
                print("targetpath: ")
                print(targetpath)       
                while (i < len(targetpath)-1):
                        src = targetpath[i]
                        dst = targetpath[i+1]
                        edgedata = topology.get_edge_data(src,dst)
                        delay = delay + edgedata['delay']
                        used_wits = edgedata['weight'] + flow2
                        #print src, dst, edgedata['weight'], flow2, used_wits
                        i=i+1
                        topology.add_weighted_edges_from([(src,dst,used_wits)]) 

            allpaths = targetpath + allpaths
        
        subtopology = topology.subgraph(allpaths)
      

        #fnss.write_topology(topology, 'topology-wax15-flow-pro.xml')
        fnss.write_topology(subtopology, 'subtopology-wax15-flow-pro.xml')
        return subtopology, delay





class RandomPath(object):
    def __init__(self, topology, tm_flow):
        self.topology = topology
        self.tm_flow = tm_flow
    def getRandomPath(self, topology, tm_flow):
        power = 0
        path_flow = tm_flow.od_pairs()[0]
        paths0 = list(nx.all_simple_paths(topology, path_flow[0], path_flow[1]))
        #print(paths0)
        print(paths0[0])
        path0 = paths0[0]
        len0 = len(path0)
        i=0
        delay = 0
        flow = tm_flow.flows()[(path_flow[0],path_flow[1])]
        while (i < len0-1):
            src = path0[i]
            dst = path0[i+1]
            delay = delay + topology.get_edge_data(src,dst)['delay']
            topology.add_weighted_edges_from([(src,dst,flow)])
            i=i+1
        k=1
        allpaths = path0
        while(k<len(tm_flow.od_pairs())):
            targetpath =[]
            path_flow = tm_flow.od_pairs()[k]
            flow2 = tm_flow.flows()[(path_flow[0],path_flow[1])]
            k=k+1
            paths2 = list(nx.all_simple_paths(topology, path_flow[0], path_flow[1]))
            #print(paths2)
            print(path_flow)
            j = 0
            while (j< len(paths2)):
                tmppath = paths2[j]
                j=j+1
                i=0
                while (i < len(tmppath)-1):
                    src = tmppath[i]
                    dst = tmppath[i+1]
                    edgedata = topology.get_edge_data(src,dst)
                    used_wits = edgedata['weight'] + flow2                
                    if(used_wits>1000):                         
                        break
                    i=i+1
                if i == len(tmppath)-1:
                    targetpath = tmppath
                    break        
            if(not len(targetpath)):##if no path with enough bandwidth
                print '**********************no enought bandwidth\n'
            else:
                print("targetpath: ")
                print(targetpath)
                i=0       
                while (i < len(targetpath)-1):
                        src = targetpath[i]
                        dst = targetpath[i+1]
                        edgedata = topology.get_edge_data(src,dst)
                        delay = delay + edgedata['delay']
                        used_wits = edgedata['weight'] + flow2
                        #print src, dst, edgedata['weight'], flow2, used_wits
                        i=i+1
                        topology.add_weighted_edges_from([(src,dst,used_wits)]) 

            allpaths = targetpath + allpaths
        
        subtopology = topology.subgraph(allpaths)
      

        #fnss.write_topology(topology, 'topology-wax15-flow-pro.xml')
        fnss.write_topology(subtopology, 'subtopology-wax15-flow-ela.xml')
        return subtopology, delay

class GreedyPath(object):
    def __init__(self, topology, tm_flow, model, enableswbase):
        self.topology = topology
        self.tm_flow = tm_flow
        self.model = model
        self.enableswbase = enableswbase
    def greenPathLookup(self, allpaths, pathlist,  model, enableswbase, flow2):
        targetpower = 10**100           
        for targetpath in pathlist: 
            tmptopology = fnss.read_topology('tmptopology-greedy.xml')  
            #tmptopology = self.topology[:]
            i=0
            while (i < len(targetpath)-1):
                src = targetpath[i]
                dst = targetpath[i+1]
                edgedata = tmptopology.get_edge_data(src,dst)
                used_wits = float(edgedata['weight']) + flow2
                i=i+1 
                #print src, dst, edgedata['weight'], used_wits                   
                tmptopology.add_weighted_edges_from([(src,dst,used_wits)]) 
            alltargetpaths = allpaths + targetpath
            tmpsubtoplogy = tmptopology.subgraph(alltargetpaths)
            power = powerCal(tmptopology, tmpsubtoplogy, model, enableswbase)
            #print targetpath, power
            if power < targetpower:
                targetpower = power
                greenpath = targetpath
        return greenpath 
            

    def getGreedyPath(self, topology, tm_flow, model, enableswbase):
        power = 0
        path_flow = tm_flow.od_pairs()[0]
        paths0 = sorted(nx.all_simple_paths(topology, path_flow[0], path_flow[1]), key=len)
        #print(paths0)
        print(paths0[0])
        path0 = paths0[0]
        len0 = len(path0)
        i=0
        delay = 0
        flow = tm_flow.flows()[(path_flow[0],path_flow[1])]
        while (i < len0-1):
            src = path0[i]
            dst = path0[i+1]
            delay = delay + topology.get_edge_data(src,dst)['delay']
            topology.add_weighted_edges_from([(src,dst,flow)])
            i=i+1
        k=1
        allpaths = path0
        alltargetpaths= path0

        while(k<len(tm_flow.od_pairs())):
            path_flow = tm_flow.od_pairs()[k]
            k=k+1
            paths2 = sorted(nx.all_simple_paths(topology, path_flow[0], path_flow[1]), key=len)
            #print(paths2)
            print(path_flow)
            pathlist = paths2[:]

            
            flow2 = tm_flow.flows()[(path_flow[0],path_flow[1])]
            for tmppath in paths2:#path with enough capacity
                i=0
                while (i < len(tmppath)-1):
                    src = tmppath[i]
                    dst = tmppath[i+1]
                    edgedata = topology.get_edge_data(src,dst)
                    used_wits = edgedata['weight'] + flow2                
                    if(used_wits>1000): 
                        pathlist.remove(tmppath)
                        break
                    i=i+1    
            print("targetpathlist with enough capacity: ")
            #print(pathlist)
            if(not len(pathlist)):##if no path with enough bandwidth
                print '**********************no enought bandwidth\n'                
                targetpath = []
            else:
                fnss.write_topology(topology, 'tmptopology-greedy.xml')  
                targetpath = self.greenPathLookup(allpaths, pathlist,  model, enableswbase, flow2)
                print targetpath
            i = 0    
            while (i < len(targetpath)-1):
                src = targetpath[i]
                dst = targetpath[i+1]
                edgedata = topology.get_edge_data(src,dst)
                #print src, dst, edgedata['weight'], used_wits
                used_wits = edgedata['weight'] + flow2
                delay = delay + edgedata['delay']
                i=i+1
                topology.add_weighted_edges_from([(src,dst,used_wits)])     

            allpaths = targetpath + allpaths
        
        subtopology = topology.subgraph(allpaths)
        #print(subtopology.edges())

        #fnss.write_topology(topology, 'topology-wax15-flow-pro.xml')
        fnss.write_topology(subtopology, 'subtopology-wax15-flow-greedy.xml')
        return subtopology, delay   



def usage():
    """
    Prints usage.
    """
    print ' [-p algorithm] [-m model][-b][-h]'
    print ' -p routing algorithm: short, priority, greedy, elastic.'
    print ' -m power model: step, linear, exponent.'
    print ' -b : only power off links and always enable switches'
    print ' -f : file name of traffic matrix'
    print ' -h :     Show help (what you are reading right now).'
    print ' -u utilization of links :    float'
    print ' -n -l number of flows: n*l   int '

def getoptions(argv):
    """
    Parses the command line options.
    """
    import getopt
    try:
        opts, args = getopt.getopt(argv, 'hp:m:f:u:n:l:b')
    except getopt.GetoptError, err:
        print err
        usage()
        sys.exit(2)
    alg    = 'short'
    model = 'linear'
    enableswbase = 1
    filename = ''
    max_u = 0.8
    n = 3
    l = 3

    for o, a in opts:
        if o == '-h':
            usage()
            sys.exit(1)
        elif o == '-p':
            alg = a
        elif o == '-m':
             model = a     
        elif o == '-b':
            enableswbase = 0
        elif o == '-f':
            filename = a
        elif o == '-u':
            max_u = float(a)
        elif o == '-n':
            n = int(a)
        elif o == '-l':
            l = int(a)
    return alg, model, enableswbase, filename , max_u, n, l    

def main(args):
    alg, model, enableswbase,filename,u,n, l  = getoptions(args)
    repeats = 10
    totalpower = 0
    totaldelay = 0
    totalpowerlist = []
    delay=0
    for repeat in xrange(repeats):  
        #topology = fnss.fat_tree_topology(4)
        topology = fnss.bcube_topology(2,3)
        fnss.set_weights_constant(topology, 1)
        fnss.set_delays_constant(topology, 1, 'ms')
        topology = topology.copy() if topology.is_directed() \
            else topology.to_directed()
        fnss.set_capacities_constant(topology, 1000, 'Mbps')

        random.seed(repeat)
        #edgenodes = [6,7,10,11,14,15,18,19]    #fattree
        edgenodes = xrange(16)      #bcube
        onodes = random.sample(edgenodes, n)
        dlist = [x for x in edgenodes if x not in onodes]
        dnodes = random.sample(dlist, l)
        #dnodes = random.sample(edgenodes, l)
        print onodes, dnodes
        tm_flow = None
        try:
            tm_flow = fnss.static_traffic_matrix(topology, mean=1000, stddev=20, max_u=None, origin_nodes=onodes,destination_nodes=dnodes)
        except:
            print 'error when generate traffic'




        subtopo = fnss.Topology()
        #nx.draw(topology)
        #plt.savefig("data/topology.png",dpi=75)
        fnss.write_traffic_matrix(tm_flow, './data/tm_flow.xml')
        fnss.set_weights_constant(topology, 0)
        fnss.write_topology(topology, './data/topology.xml')
        

        
        #print(tm_flow.flows())
        #print(tm_flow.od_pairs())
        print alg
        if fnss.validate_traffic_matrix(topology, tm_flow):
            if alg =='short':
                shortpath = ShortestPath(topology, tm_flow)
                subtopo, delay = shortpath.getShortestPath(topology, tm_flow)
            elif alg == 'priority':
                propath = PriorityPath(topology, tm_flow)
                subtopo, delay = propath.getPriorityPath(topology, tm_flow)
            elif alg == 'greedy':
                greedypath = GreedyPath(topology, tm_flow, model, enableswbase)   
                subtopo, delay = greedypath.getGreedyPath(topology, tm_flow, model, enableswbase) 
            elif alg == 'elastic':
                randompath = RandomPath(topology, tm_flow)   
                subtopo, delay = randompath.getRandomPath(topology, tm_flow) 
        else:
            print 'error when validate traffic'
           

        power = powerCal(topology, subtopo, model, enableswbase)
        print power
        totalpowerlist.append(power)
        totalpower = totalpower + power
        numedges = 0
        # for e in subtopo.edges(data=True):
        #     if e[2]['weight']:
        #         numedges +=1

        avgdelay = delay/float(n*l)
        totaldelay = totaldelay + avgdelay 
    powerarray =  np.array(totalpowerlist)
    powerstd = np.std(powerarray)
    powermean = np.mean(powerarray)
    print  powermean  
    row = [alg, model, enableswbase, u, filename, powermean, totaldelay/repeats, n, l, powerstd]
    with open('./data/routing-bcube-m2m-1.0.csv', "a") as fp:
        fdata = csv.writer(fp, delimiter=',')
        #fdata.writerow(head)
        fdata.writerow(row)

if __name__ == '__main__':
    main(sys.argv[1:])
