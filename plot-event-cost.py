"""
Plot the energy efficiency and completion time against the time of switching device's state for BCube or Fat Tree networks
"""



import csv
import numpy as np
import sys
import matplotlib.pyplot as plt
import math

def algfilter(dataarray, alg, num, scale):
	algcol = dataarray[:,0]
	modelcol = dataarray[:,1]
	shortcols = dataarray[algcol==alg]
	fullcol = shortcols[:,10]
	shortEnergy = shortcols[fullcol=='False'][:,num]
	shortfullEnergy = shortcols[fullcol=='True'][:,num]
	if scale !=-1:
		shortEnergy = [float(val)/scale for val in shortEnergy]
		shortfullEnergy = [float(val)/scale for val in shortfullEnergy]
	else:	
		shortEnergy = [1/float(val) for val in shortEnergy]
		shortfullEnergy = [1/float(val) for val in shortfullEnergy]
	print alg, shortEnergy, shortfullEnergy
	return shortEnergy, shortfullEnergy

def main(argv):
	data = np.genfromtxt('./data/homo/cost/routing-event-cost-bcube.csv', delimiter=','  ,\
		dtype=None, skip_header=0)
	

	dataarray = np.asarray([list(i) for i in data])

	fattreedata = np.genfromtxt('./data/homo/cost/routing-event-cost-fattree.csv', delimiter=','  ,\
		dtype=None, skip_header=0)

	fattreedataarray = np.asarray([list(i) for i in fattreedata])
	#fattree
	# totalpower = 120*20+2*4*16
	# totalpower3 = 120*20+(0.01*math.pow(1000,0.6667)+1)*4*16
	#wax
	# totalpower = 120*36+2*4*36
	# totalpower3 = 120*36+(0.01*math.pow(1000,0.6667)+1)*4*36
	# #bcube
	totalpower = 120*32+2*2*32
	totalpower3 = 120*32+(0.01*math.pow(1000,0.6667)+1)*2*32
	# x0 = 1
	# x1 = 5
	# x2 = 10
	# x3 = 20
	# x4 = 50
	# x5 = 100

	x0 = 0.0001#0.1
	x1 = 2
	x2 = 5
	x3 = 10
	x4 = 15
	x5 = 20
	x6 = 25
	x7 = 30
	labelx = 'Transition time'

	x = [ x0, x1, x2, x3, x4, x5, x6 ,x7]
	KJoulescale = -1#1000
	axx= 30 
	Energyaxy = 7#1100 
	Timeaxy = 180 

	numEnergy = 15 #5
	shortEnergy, shortfullEnergy = algfilter(dataarray, 'short', numEnergy,KJoulescale)
	proEnergy, profullEnergy = algfilter(dataarray, 'priority',numEnergy,KJoulescale)
	randomEnergy, randomfullEnergy = algfilter(dataarray, 'elastic', numEnergy, KJoulescale)
	greedyEnergy, greedyfullEnergy = algfilter(dataarray, 'greedy', numEnergy, KJoulescale)


	fig = plt.figure(1,(9,4))
	plt.subplot(1,2,1)
	plt.plot(x,shortEnergy,'b*-',ms=4, label='short')
	plt.plot(x,shortfullEnergy,'b*:', ms=4,label='short+EXR')

	plt.plot(x,proEnergy,'kx-', label='priority')
	plt.plot(x,profullEnergy,'kx:', label='priority+EXR')
	
	plt.plot(x,randomEnergy,'ro-', ms=4,label='random')
	plt.plot(x,randomfullEnergy,'ro:',ms=4, label='random+EXR')
	
	plt.plot(x,greedyEnergy,'g+-', label='greedy')
	plt.plot(x,greedyfullEnergy,'g+:', label='greedy+EXR')

	plt.legend(prop={'size':8}, loc='lower right')
	plt.title('(a) BCube')
 	#plt.ylabel('Network energy(KJ)')
 	plt.ylabel('Network Energy Efficiency(MB/J)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
 	plt.axis((0,axx,0,Energyaxy)) 
	#plt.axis((0,20,0,150)) 



	shortEnergy, shortfullEnergy = algfilter(fattreedataarray, 'short', numEnergy,KJoulescale)
	proEnergy, profullEnergy = algfilter(fattreedataarray, 'priority',numEnergy,KJoulescale)
	randomEnergy, randomfullEnergy = algfilter(fattreedataarray, 'elastic', numEnergy, KJoulescale)
	greedyEnergy, greedyfullEnergy = algfilter(fattreedataarray, 'greedy', numEnergy, KJoulescale)

	plt.subplot(1,2,2)
	plt.plot(x,shortEnergy,'b*-', ms=4,label='short')
	plt.plot(x,shortfullEnergy,'b*:', ms=4,label='short+EXR')

	plt.plot(x,proEnergy,'kx-', label='priority')
	plt.plot(x,profullEnergy,'kx:', label='priority+EXR')
	
	plt.plot(x,randomEnergy,'ro-', ms=4,label='random')
	plt.plot(x,randomfullEnergy,'ro:',ms=4, label='random+EXR')
	
	plt.plot(x,greedyEnergy,'g+-', label='greedy')
	plt.plot(x,greedyfullEnergy,'g+:', label='greedy+EXR')

	plt.legend(prop={'size':8}, loc='lower right')
	plt.title('(b) Fat Tree')
 	#plt.ylabel('Network energy(KJ)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
	plt.axis((0,axx,0,Energyaxy)) 
	#plt.axis((0,20,0,150)) 

	plt.savefig('./data/event-ee-cost.pdf')





	fig2 = plt.figure(2,(9,4))

	numTime = 11
	timescale = 1
	shortTime, shortfullTime = algfilter(dataarray, 'short', numTime,timescale)
	proTime, profullTime = algfilter(dataarray, 'priority',numTime, timescale)
	randomTime, randomfullTime = algfilter(dataarray, 'elastic', numTime, timescale)
	greedyTime, greedyfullTime = algfilter(dataarray, 'greedy', numTime, timescale)
	plt.subplot(1,2,1)


	plt.plot(x,shortTime,'b*-',label='short')
	plt.plot(x,shortfullTime,'b*:', ms= 4,label='short+EXR')

	plt.plot(x,proTime,'kx-', label='priority')
	plt.plot(x,profullTime,'kx:', label='priority+EXR')
	
	plt.plot(x,randomTime,'ro-', ms= 4.0,label='random')
	plt.plot(x,randomfullTime,'ro:', ms= 4.0, label='random+EXR')
	
	plt.plot(x,greedyTime,'g+-', label='greedy')
	plt.plot(x,greedyfullTime,'g+:', label='greedy+EXR')

	plt.legend(prop={'size':8}, loc='upper left')
	plt.title('(a) BCube')
 	plt.ylabel('Mean flow completion time(s)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
	plt.axis((0,axx,0,Timeaxy)) 
	#plt.axis((0,20,0,0.8)) 


	shortTime, shortfullTime = algfilter(fattreedataarray, 'short', numTime,timescale)
	proTime, profullTime = algfilter(fattreedataarray, 'priority',numTime, timescale)
	randomTime, randomfullTime = algfilter(fattreedataarray, 'elastic', numTime, timescale)
	greedyTime, greedyfullTime = algfilter(fattreedataarray, 'greedy', numTime, timescale)

	plt.subplot(1,2,2)


	plt.plot(x,shortTime,'b*-', label='short')
	plt.plot(x,shortfullTime,'b*:',ms= 4.0, label='short+EXR')

	plt.plot(x,proTime,'kx-', label='priority')
	plt.plot(x,profullTime,'kx:',label='priority+EXR')
	
	plt.plot(x,randomTime,'ro-', ms=4,label='random')
	plt.plot(x,randomfullTime,'ro:', ms=4, label='random+EXR')
	
	plt.plot(x,greedyTime,'g+-', label='greedy')
	plt.plot(x,greedyfullTime,'g+:', label='greedy+EXR')

	plt.legend(prop={'size':8}, loc='upper left')
	plt.title('(b) Fat Tree')
 	plt.ylabel('Mean flow completion time(s)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
 	plt.axis((0,axx,0,Timeaxy)) 
	#plt.axis((0,20,0,0.8)) 

	plt.savefig('./data/event-time-cost.pdf')



	
if __name__ == '__main__':
    sys.exit(main(sys.argv))