# plot the power consumption and the delay of three routing algorithms using a linear power model: 
# shortest (linear), greedy (lineargreedy), randow (linearela)

import csv
import numpy as np
import sys
import matplotlib.pyplot as plt
import math

def algfilter(dataarray, alg):
	algcol = dataarray[:,0]
	modelcol = dataarray[:,1]
	shortcols = dataarray[algcol==alg]
	powercol = shortcols[:,5]
	powererrorcol = shortcols[:,9]
	delaycol = shortcols[:,6]

	#fattree
	# totalpower = 120*20+2*4*16
	# totalpower3 = 120*20+(0.01*math.pow(1000,0.6667)+1)*4*16
	totalpower = 120*32+2*2*16

	powercol = [float(val)/totalpower for val in powercol]
	powererrorcol = [float(val)/totalpower for val in powererrorcol]
	delaycol = [float(val) for val in delaycol]
	print powercol

	return powercol, powererrorcol, delaycol

def main(argv):
	data = np.genfromtxt('./data/routing-bcube-m2m-1.0.csv', delimiter=','  ,dtype=None, skip_header=0)	

	dataarray = np.asarray([list(i) for i in data])



	shortp, shortpe, shortd = algfilter(dataarray, 'short')
	greedyp, greedype, greedyd = algfilter(dataarray, 'greedy')
	elap, elape, elad = algfilter(dataarray, 'elastic')

	x1 = 0.25
	x2 = 6.0/16.0
	x3 = 8.0/16.0
	x4 = 12.0/16.0
	x5 = 1.0

	# x1 = 4
	# x2 = 6
	# x3 = 9
	# x4 = 12
	# x5 = 16
	x = [ x1, x2, x3, x4, x5]
	
																				



	fig=plt.figure(1)
	linear = plt.errorbar(x,shortp,yerr=shortpe,fmt='b*:', label='shortest')
	lineargreedy = plt.errorbar(x,greedyp, yerr=greedype,fmt='y--', label='greedy')
	linearela = plt.errorbar(x,elap,yerr=elape,fmt='go-', label='random')
	plt.legend(prop={'size':10}, loc='upper left')
	ax1,ax2,ay1,ay2 = plt.axis()
	plt.axis((0,1,0,1)) 
	plt.ylabel('% of original power')
	plt.xlabel('Network utilization')
	plt.savefig('./data/flownumlinear-overload-3alg.pdf')




	fig=plt.figure(2)
	linear = plt.plot(x,shortd,'b*:', label='shortest')
	lineargreedy = plt.plot(x,greedyd,'y--', label='greedy')
	linearela = plt.plot(x,elad,'go-', label='random')
	plt.legend(prop={'size':10}, loc='upper left')
	ax1,ax2,ay1,ay2 = plt.axis()
	plt.axis((0,1,0,16)) 
	plt.ylabel('Mean flow delay (ms)')
	plt.xlabel('Network utilization')
	plt.savefig('./data/lineardelay-3alg.pdf')
	#plt.show()


if __name__ == '__main__':
    sys.exit(main(sys.argv))