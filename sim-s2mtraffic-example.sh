#!/bin/bash


python simulator_bcube.py -p short -m exponent -n 2 -l 1

python simulator_bcube.py -p short -m exponent -n 5 -l 1

python simulator_bcube.py -p short -m exponent -n 10 -l 1

python simulator_bcube.py -p short -m exponent -n 12 -l 1

python simulator_bcube.py -p short -m exponent -n 15 -l 1



python simulator_bcube.py -p short -m step -n 2 -l 1

python simulator_bcube.py -p short -m step -n 5 -l 1

python simulator_bcube.py -p short -m step -n 10 -l 1

python simulator_bcube.py -p short -m step -n 12 -l 1

python simulator_bcube.py -p short -m step -n 15 -l 1





python simulator_bcube.py -p short -m linear -n 2 -l 1

python simulator_bcube.py -p short -m linear -n 5 -l 1

python simulator_bcube.py -p short -m linear -n 10 -l 1

python simulator_bcube.py -p short -m linear -n 12 -l 1

python simulator_bcube.py -p short -m linear -n 15 -l 1




python simulator_bcube.py -p priority -m exponent -n 2 -l 1

python simulator_bcube.py -p priority -m exponent -n 5 -l 1

python simulator_bcube.py -p priority -m exponent -n 10 -l 1

python simulator_bcube.py -p priority -m exponent -n 12 -l 1

python simulator_bcube.py -p priority -m exponent -n 15 -l 1



python simulator_bcube.py -p priority -m step -n 2 -l 1

python simulator_bcube.py -p priority -m step -n 5 -l 1

python simulator_bcube.py -p priority -m step -n 10 -l 1

python simulator_bcube.py -p priority -m step -n 12 -l 1

python simulator_bcube.py -p priority -m step -n 15 -l 1




python simulator_bcube.py -p priority -m linear -n 1

python simulator_bcube.py -p priority -m linear -n 2

python simulator_bcube.py -p priority -m linear -n 5

python simulator_bcube.py -p priority -m linear -n 10

python simulator_bcube.py -p priority -m linear -n 12

python simulator_bcube.py -p priority -m linear -n 15




python simulator_bcube.py -p elastic -m exponent -n 1

python simulator_bcube.py -p greedy -m exponent -n 2

python simulator_bcube.py -p greedy -m exponent -n 5

python simulator_bcube.py -p greedy -m exponent -n 10

python simulator_bcube.py -p greedy -m exponent -n 12

python simulator_bcube.py -p greedy -m exponent -n 15



python simulator_bcube.py -p greedy -m step -n 1

python simulator_bcube.py -p greedy -m step -n 2

python simulator_bcube.py -p greedy -m step -n 5

python simulator_bcube.py -p greedy -m step -n 10

python simulator_bcube.py -p greedy -m step -n 12

python simulator_bcube.py -p greedy -m step -n 15



python simulator_bcube.py -p greedy -m linear -n 1

python simulator_bcube.py -p greedy -m linear -n 2

python simulator_bcube.py -p greedy -m linear -n 5

python simulator_bcube.py -p greedy -m linear -n 10

python simulator_bcube.py -p greedy -m linear -n 12

python simulator_bcube.py -p greedy -m linear -n 15


python simulator_bcube.py -p elastic -m exponent -n 1

python simulator_bcube.py -p elastic -m exponent -n 2

python simulator_bcube.py -p elastic -m exponent -n 5

python simulator_bcube.py -p elastic -m exponent -n 10

python simulator_bcube.py -p elastic -m exponent -n 12

python simulator_bcube.py -p elastic -m exponent -n 15



python simulator_bcube.py -p elastic -m step -n 1

python simulator_bcube.py -p elastic -m step -n 2

python simulator_bcube.py -p elastic -m step -n 5

python simulator_bcube.py -p elastic -m step -n 10

python simulator_bcube.py -p elastic -m step -n 12

python simulator_bcube.py -p elastic -m step -n 15



python simulator_bcube.py -p elastic -m linear -n 1

python simulator_bcube.py -p elastic -m linear -n 2

python simulator_bcube.py -p elastic -m linear -n 5

python simulator_bcube.py -p elastic -m linear -n 10

python simulator_bcube.py -p elastic -m linear -n 12

python simulator_bcube.py -p elastic -m linear -n 15


