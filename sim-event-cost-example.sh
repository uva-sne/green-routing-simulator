#!/bin/bash


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 0.1

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 0.1

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 0.1

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 0.1


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 0.1 -e

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 0.1 -e

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 0.1 -e

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 0.1 -e



python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 2

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 2

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 2

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 2


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 2 -e

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 2 -e

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 2 -e

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 2 -e





python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 5

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 5

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 5

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 5


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 5 -e

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 5 -e

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 5 -e

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 5 -e




python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 10

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 10

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 10

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 10


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 10 -e

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 10 -e

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 10 -e

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 10 -e



python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 15

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 15

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 15

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 15


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 15 -e

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 15 -e

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 15 -e

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 15 -e



python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c  20

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 20

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 20

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 20


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 20 -e

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 20 -e

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 20 -e

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 20 -e


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 25

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 25

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 25

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 25


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 25 -e

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 25 -e

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 25 -e

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 25 -e


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c  30

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 30

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 30

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 30


python simulator_event_transition_cost.py -p greedy -m linear -n 1 -l 1  -r 1 -c 30 -e

python simulator_event_transition_cost.py -p elastic -m linear -n 1 -l 1  -r 1 -c 30 -e

python simulator_event_transition_cost.py -p short -m linear -n 1 -l 1  -r 1 -c 30 -e

python simulator_event_transition_cost.py -p priority -m linear -n 1 -l 1  -r 1 -c 30 -e







