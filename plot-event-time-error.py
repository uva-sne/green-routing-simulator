"""
Plot the completion time with errors against arrival rate for BCube or Fat Tree networks
"""


import csv
import numpy as np
import sys
import matplotlib.pyplot as plt
import math

def algfilter(dataarray, alg, num, scale):
	algcol = dataarray[:,0]
	modelcol = dataarray[:,1]
	shortcols = dataarray[algcol==alg]
	fullcol = shortcols[:,10]
	shortEnergy = shortcols[fullcol=='False'][:,num]
	shortfullEnergy = shortcols[fullcol=='True'][:,num]
	if scale != -1:
		shortEnergy = [float(val)/scale for val in shortEnergy]
		shortfullEnergy = [float(val)/scale for val in shortfullEnergy]
	else:	
		shortEnergy = [1/float(val) for val in shortEnergy]
		shortfullEnergy = [1/float(val) for val in shortfullEnergy]
	#print alg, shortEnergy, shortfullEnergy
	return shortEnergy, shortfullEnergy


def errorfilter(dataarray, alg, num, scale):
	x, xx = algfilter(dataarray, alg, 16, -1)
	std, std2 = algfilter(dataarray, alg, num, 1)
	se = []
	se2 = []
	for k in range(len(x)):
		se.append(std[k]/math.sqrt(x[k]))
	for k in range(len(xx)):
		se2.append(std2[k]/math.sqrt(xx[k]))
	print std, se	
	return se, se2	

def main(argv):
	data = np.genfromtxt('./data/homo/routing-event-bcube-big-uniform-std.csv', delimiter=','  ,\
		dtype=None, skip_header=0)
	

	dataarray = np.asarray([list(i) for i in data])

	fattreedata = np.genfromtxt('./data/homo/routing-event-fattree-big-uniform-std.csv', delimiter=','  ,\
		dtype=None, skip_header=0)

	fattreedataarray = np.asarray([list(i) for i in fattreedata])
	#fattree
	# totalpower = 120*20+2*4*16
	# totalpower3 = 120*20+(0.01*math.pow(1000,0.6667)+1)*4*16
	#wax
	# totalpower = 120*36+2*4*36
	# totalpower3 = 120*36+(0.01*math.pow(1000,0.6667)+1)*4*36
	# #bcube
	totalpower = 120*32+2*2*32
	totalpower3 = 120*32+(0.01*math.pow(1000,0.6667)+1)*2*32
	# x0 = 1
	# x1 = 5
	# x2 = 10
	# x3 = 20
	# x4 = 50
	# x5 = 100

	x0 = 50
	x1 = 20
	x2 = 10
	x3 = 5
	x4 = 1
	x5 = 0.1
	labelx = 'Flow arrival rate(flows/s)'

	#x = [ x0, x1, x2, x3, x4]
	#x = [ x1, x2, x3, x4, x5]
	x, xx = algfilter(dataarray, 'short', 16, -1)
	KJoulescale = -1
	axx=   22 #55
	Energyaxy =  7#8 
	Timeaxy = 200#0.25
	location = 'lower right'#'upper left'

	numEnergy = 15
	shortEnergy, shortfullEnergy = algfilter(dataarray, 'short', numEnergy,KJoulescale)
	proEnergy, profullEnergy = algfilter(dataarray, 'priority',numEnergy,KJoulescale)
	randomEnergy, randomfullEnergy = algfilter(dataarray, 'elastic', numEnergy, KJoulescale)
	greedyEnergy, greedyfullEnergy = algfilter(dataarray, 'greedy', numEnergy, KJoulescale)


	fig = plt.figure(1,(9,4))

	#plt.axis((0,20,0,150)) 

	numTime = 11
	timescale = 1
	shortTime, shortfullTime = algfilter(dataarray, 'short', numTime,timescale)
	proTime, profullTime = algfilter(dataarray, 'priority',numTime, timescale)
	randomTime, randomfullTime = algfilter(dataarray, 'elastic', numTime, timescale)
	greedyTime, greedyfullTime = algfilter(dataarray, 'greedy', numTime, timescale)

	numSE = 12
	shortSE, shortfullSE = errorfilter(dataarray, 'short', numSE,timescale)
	proSE, profullSE = errorfilter(dataarray, 'priority',numSE, timescale)
	randomSE, randomfullSE = errorfilter(dataarray, 'elastic', numSE, timescale)
	greedySE, greedyfullSE = errorfilter(dataarray, 'greedy', numSE, timescale)




 	N=2
	ind=np.arange(N)
	width = 0.05
	fig,ax = plt.subplots()
	rects1 = ax.bar(ind, shortTime[1:3],  width,color='y', yerr=shortSE[1:3])
	rects1full = ax.bar(ind+width, shortfullTime[1:3],  width,color='w', hatch = 'o',yerr=shortfullSE[1:3])

	rects2 = ax.bar(ind+width*2, proTime[1:3], width, color='w',yerr=proSE[1:3])
	rects2full = ax.bar(ind+width*3, profullTime[1:3], width, color='w', hatch = 'O',yerr=profullSE[1:3])

	rects3 = ax.bar(ind+width*4, randomTime[1:3], width, color='w', hatch = '/', yerr=randomSE[1:3])
	rects3full = ax.bar(ind+width*5, randomfullTime[1:3], width, color='w',hatch = '//', yerr=randomfullSE[1:3])

	rects4 = ax.bar(ind+width*6, greedyTime[1:3], width, color='g', yerr=greedySE[1:3])
	rects4full = ax.bar(ind+width*7, greedyfullTime[1:3], width, color='w',hatch = '\\', yerr=greedyfullSE[1:3])
	ax.set_xticks(ind+width*3)
	ax.set_xlabel('Flow arrival rate(flows/s)')
	ax.set_xticklabels(('20','    10'))
	ax.set_ylabel('Mean flow completion time(s)')
	ax.set_title('BCube')
	ax.set_ylim([0,100])

	ax.legend((rects1[0],rects1full[0], rects2[0],rects2full[0], rects3[0],rects3full[0],rects4[0],  rects4full[0] ), 
		('short', 'short+EXR','priority',  'priority+EXR','random', 'random+EXR','greedy', 'greedy+EXR'), loc='upper right')





	plt.savefig('./data/bcube-time-bar.pdf')









if __name__ == '__main__':
    sys.exit(main(sys.argv))