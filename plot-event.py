"""
Plot the energy consumption and completion time against arrival rate for BCube or Fat Tree networks
"""



import csv
import numpy as np
import sys
import matplotlib.pyplot as plt
import math

def algfilter(dataarray, alg, num, scale):
	algcol = dataarray[:,0]
	modelcol = dataarray[:,1]
	shortcols = dataarray[algcol==alg]
	fullcol = shortcols[:,10]
	shortEnergy = shortcols[fullcol=='False'][:,num]
	shortfullEnergy = shortcols[fullcol=='True'][:,num]
	shortEnergy = [float(val)/scale for val in shortEnergy]
	shortfullEnergy = [float(val)/scale for val in shortfullEnergy]
	print alg, shortEnergy, shortfullEnergy
	return shortEnergy, shortfullEnergy

def main(argv):
	data = np.genfromtxt('./data/homo/routing-event-bcube-small-uniform.csv', delimiter=','  ,\
		dtype=None, skip_header=0)
	

	dataarray = np.asarray([list(i) for i in data])

	fattreedata = np.genfromtxt('./data/homo/routing-event-fattree-small-uniform.csv', delimiter=','  ,\
		dtype=None, skip_header=0)

	fattreedataarray = np.asarray([list(i) for i in fattreedata])
	#fattree
	# totalpower = 120*20+2*4*16
	# totalpower3 = 120*20+(0.01*math.pow(1000,0.6667)+1)*4*16
	#wax
	# totalpower = 120*36+2*4*36
	# totalpower3 = 120*36+(0.01*math.pow(1000,0.6667)+1)*4*36
	# #bcube
	totalpower = 120*32+2*2*32
	totalpower3 = 120*32+(0.01*math.pow(1000,0.6667)+1)*2*32
	# x0 = 1
	# x1 = 5
	# x2 = 10
	# x3 = 20
	# x4 = 50
	# x5 = 100

	x0 = 50
	x1 = 20
	x2 = 10
	x3 = 5
	x4 = 1
	x5 = 0.1
	labelx = 'Flow arrival rate(flows/s)'

	x = [ x0, x1, x2, x3, x4]
	KJoulescale = 1000
	axx= 55 # 22
	Energyaxy = 160#1500
	Timeaxy = 0.8#200

	numEnergy = 5
	shortEnergy, shortfullEnergy = algfilter(dataarray, 'short', numEnergy,KJoulescale)
	proEnergy, profullEnergy = algfilter(dataarray, 'priority',numEnergy,KJoulescale)
	randomEnergy, randomfullEnergy = algfilter(dataarray, 'elastic', numEnergy, KJoulescale)
	greedyEnergy, greedyfullEnergy = algfilter(dataarray, 'greedy', numEnergy, KJoulescale)


	fig = plt.figure(1,(9,4))
	plt.subplot(1,2,1)
	plt.plot(x,shortEnergy,'b*-',ms=4, label='short')
	plt.plot(x,shortfullEnergy,'b*:', ms=4,label='short + EXR')

	plt.plot(x,proEnergy,'kx-', label='priority')
	plt.plot(x,profullEnergy,'kx:', label='priority + EXR')
	
	plt.plot(x,randomEnergy,'ro-', ms=4,label='random')
	plt.plot(x,randomfullEnergy,'ro:',ms=4, label='random + EXR')
	
	plt.plot(x,greedyEnergy,'g+-', label='greedy')
	plt.plot(x,greedyfullEnergy,'g+:', label='greedy + EXR')

	plt.legend(prop={'size':8}, loc='upper left')
	plt.title('BCube')
 	plt.ylabel('Network energy(KJ)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
 	plt.axis((0,axx,0,Energyaxy)) 
	#plt.axis((0,20,0,150)) 



	shortEnergy, shortfullEnergy = algfilter(fattreedataarray, 'short', numEnergy,KJoulescale)
	proEnergy, profullEnergy = algfilter(fattreedataarray, 'priority',numEnergy,KJoulescale)
	randomEnergy, randomfullEnergy = algfilter(fattreedataarray, 'elastic', numEnergy, KJoulescale)
	greedyEnergy, greedyfullEnergy = algfilter(fattreedataarray, 'greedy', numEnergy, KJoulescale)

	plt.subplot(1,2,2)
	plt.plot(x,shortEnergy,'b*-', ms=4,label='short')
	plt.plot(x,shortfullEnergy,'b*:', ms=4,label='short + EXR')

	plt.plot(x,proEnergy,'kx-', label='priority')
	plt.plot(x,profullEnergy,'kx:', label='priority + EXR')
	
	plt.plot(x,randomEnergy,'ro-', ms=4,label='random')
	plt.plot(x,randomfullEnergy,'ro:',ms=4, label='random + EXR')
	
	plt.plot(x,greedyEnergy,'g+-', label='greedy')
	plt.plot(x,greedyfullEnergy,'g+:', label='greedy + EXR')

	plt.legend(prop={'size':8}, loc='upper left')
	plt.title('Fat Tree')
 	#plt.ylabel('Network energy(KJ)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
	plt.axis((0,axx,0,Energyaxy)) 
	#plt.axis((0,20,0,150)) 

	plt.savefig('./data/event-energy.pdf')





	fig2 = plt.figure(2,(9,4))

	numTime = 11
	timescale = 1
	shortTime, shortfullTime = algfilter(dataarray, 'short', numTime,timescale)
	proTime, profullTime = algfilter(dataarray, 'priority',numTime, timescale)
	randomTime, randomfullTime = algfilter(dataarray, 'elastic', numTime, timescale)
	greedyTime, greedyfullTime = algfilter(dataarray, 'greedy', numTime, timescale)
	plt.subplot(1,2,1)


	plt.plot(x,shortTime,'b*-',label='short')
	plt.plot(x,shortfullTime,'b*:', ms= 4,label='short + EXR')

	plt.plot(x,proTime,'kx-', label='priority')
	plt.plot(x,profullTime,'kx:', label='priority + EXR')
	
	plt.plot(x,randomTime,'ro-', ms= 4.0,label='random')
	plt.plot(x,randomfullTime,'ro:', ms= 4.0, label='random + EXR')
	
	plt.plot(x,greedyTime,'g+-', label='greedy')
	plt.plot(x,greedyfullTime,'g+:', label='greedy + EXR')

	plt.legend(prop={'size':8}, loc='upper left')
	plt.title(' BCube')
 	plt.ylabel('Mean flow completion time(s)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
	plt.axis((0,axx,0,Timeaxy)) 
	#plt.axis((0,20,0,0.8)) 


	shortTime, shortfullTime = algfilter(fattreedataarray, 'short', numTime,timescale)
	proTime, profullTime = algfilter(fattreedataarray, 'priority',numTime, timescale)
	randomTime, randomfullTime = algfilter(fattreedataarray, 'elastic', numTime, timescale)
	greedyTime, greedyfullTime = algfilter(fattreedataarray, 'greedy', numTime, timescale)

	plt.subplot(1,2,2)


	plt.plot(x,shortTime,'b*-', label='short')
	plt.plot(x,shortfullTime,'b*:',ms= 4.0, label='short + EXR')

	plt.plot(x,proTime,'kx-', label='priority')
	plt.plot(x,profullTime,'kx:',label='priority + EXR')
	
	plt.plot(x,randomTime,'ro-', ms=4,label='random')
	plt.plot(x,randomfullTime,'ro:', ms=4, label='random + EXR')
	
	plt.plot(x,greedyTime,'g+-', label='greedy')
	plt.plot(x,greedyfullTime,'g+:', label='greedy + EXR')

	plt.legend(prop={'size':8}, loc='upper left')
	plt.title(' Fattree')
 	plt.ylabel('Mean flow completion time(s)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
 	plt.axis((0,axx,0,Timeaxy)) 
	#plt.axis((0,20,0,0.8)) 

	plt.savefig('./data/event-time.pdf')



	# fig3 =plt.figure(3)
	# fig3,ax3 = plt.subplots()
	# numEE = 15
	# shortEE, shortfullEE = algfilter(dataarray, 'short', numEE)
	# proEE, profullEE = algfilter(dataarray, 'priority',numEE)
	# randomEE, randomfullEE = algfilter(dataarray, 'elastic', numEE)
	# greedyEE, greedyfullEE = algfilter(dataarray, 'greedy', numEE)

	# N=5
	# ind=np.arange(N)
	# width = 0.1
	# fig,ax = plt.subplots()
	# rects1 = ax.bar(ind, shortEE, width,color='r')
	# rects2 = ax.bar(ind+width, proEE, width, color='y')
	# rects3 = ax.bar(ind+width*2, randomEE, width, color='k')
	# rects4 = ax.bar(ind+width*3, greedyEE, width, color='g')
	# ax.set_xticks(ind+width)
	# ax.set_xticklabels(('5', '10','20','50','100'))
	# ax.set_ylabel('Energy Efficency(Joule/MB)')
	# ax.set_title('bcube(2,3)')
	# ax.set_ylim([0,2])

	# ax.legend((rects1[0], rects2[0], rects3[0],rects4), ('short', 'priority', 'random','greedy'), loc='upper right')
	# plt.savefig('./data/event-bar.pdf')


	# fig=plt.figure(1)
	# pos=0
	# # enablelisty = [steplisty[pos], linearlisty[pos], exponentlisty[pos], listy[pos]]
	# # disablelisty = [stepprolisty[4], linearprolisty[4], exponentprolisty[4], prolisty[4]]

	# # enableerror = [errorshort[4], errorpro[4], errorgreedy[4], errorela[4]]
	# # disenableerror = [diserrorshort[4], diserrorpro[4], diserrorgreedy[4], diserrorela[4]]

	# plt.figure(1)
	# N=2
	# ind=np.arange(N)
	# width = 0.15
	# print steplisty, linearlisty
	# fig,ax = plt.subplots()
	# rects1 = ax.bar(ind, [steplisty[0],linearlisty[0]], width,color='r')
	# rects2 = ax.bar(ind+width, [steplisty[1],linearlisty[1]], width, color='y')
	# rects3 = ax.bar(ind+width*2, [steplisty[2],linearlisty[2]], width, color='k')
	# ax.set_xticks(ind+width)
	# ax.set_xticklabels(('short', 'priority'))
	# ax.set_ylabel('% of original power')
	# ax.set_title('bcube(8,2) topology, 100*100 flows')
	# ax.set_ylim([0,1])

	# ax.legend((rects1[0], rects2[0], rects3[0]), ('exponent', 'step', 'linear'), loc='upper right')
	# plt.savefig('./data/bigtology-bar.png')

	

# #figure 2
# 	plt.figure(2)
# 	step = plt.plot(steplistx,steplisty,'b', label='short (switch+link)')
# 	steppro = plt.plot(stepprolistx,stepprolisty,'b:', label='short (link)', lw=2)


# 	linear = plt.plot(linearlistx,linearlisty,'r', label='priority (switch+link)')
# 	linearpro = plt.plot(linearprolistx,linearprolisty,'r:', label='priority (link)', lw =2)

# 	exponent = plt.plot(exponentlistx,exponentlisty,'y', label='greedy (switch+link)')
# 	exponentpro = plt.plot(exponentprolistx,exponentprolisty,'y:', label='greedy (link)', lw=2)

# 	elastic = plt.plot(listx,listy,'g', label='elastic tree (switch+link)')
# 	elasticpro = plt.plot(prolistx,prolisty,'g:', label='elastic tree (link)', lw =2)
	
# 	plt.legend(prop={'size':10}, loc='upper left')
# 	x1,x2,y1,y2 = plt.axis()
# 	plt.axis((0,1,0,1)) 
# 	plt.title(' linear router')
# 	plt.ylabel('% original power')
# 	plt.xlabel('maximum link utilization')
# 	plt.savefig("data/vsthoughput.png")


if __name__ == '__main__':
    sys.exit(main(sys.argv))