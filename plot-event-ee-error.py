"""
Plot the energy efficiency with errors against arrival rate for BCube or Fat Tree networks
"""



import csv
import numpy as np
import sys
import matplotlib.pyplot as plt
import math

def algfilter(dataarray, alg, num, scale):
	algcol = dataarray[:,0]
	modelcol = dataarray[:,1]
	shortcols = dataarray[algcol==alg]
	fullcol = shortcols[:,10]
	shortEnergy = shortcols[fullcol=='False'][:,num]
	shortfullEnergy = shortcols[fullcol=='True'][:,num]
	if scale != -1:
		shortEnergy = [float(val)/scale for val in shortEnergy]
		shortfullEnergy = [float(val)/scale for val in shortfullEnergy]
	else:	
		shortEnergy = [1/float(val) for val in shortEnergy]
		shortfullEnergy = [1/float(val) for val in shortfullEnergy]
	#print alg, shortEnergy, shortfullEnergy
	return shortEnergy, shortfullEnergy


def errorfilter(dataarray, alg, num, scale):
	x, xx = algfilter(dataarray, alg, 16, -1)
	std, std2 = algfilter(dataarray, alg, num, 1)
	se = []
	se2 = []
	for k in range(len(x)):
		se.append(std[k]/math.sqrt(x[k]))
	for k in range(len(xx)):
		se2.append(std2[k]/math.sqrt(xx[k]))
	print std, se	
	return se, se2	

def main(argv):
	data = np.genfromtxt('./data/homo/routing-event-bcube-big-uniform-std.csv', delimiter=','  ,\
		dtype=None, skip_header=0)
	

	dataarray = np.asarray([list(i) for i in data])

	fattreedata = np.genfromtxt('./data/homo/routing-event-fattree-big-uniform-std.csv', delimiter=','  ,\
		dtype=None, skip_header=0)

	fattreedataarray = np.asarray([list(i) for i in fattreedata])
	#fattree
	# totalpower = 120*20+2*4*16
	# totalpower3 = 120*20+(0.01*math.pow(1000,0.6667)+1)*4*16
	#wax
	# totalpower = 120*36+2*4*36
	# totalpower3 = 120*36+(0.01*math.pow(1000,0.6667)+1)*4*36
	# #bcube
	totalpower = 120*32+2*2*32
	totalpower3 = 120*32+(0.01*math.pow(1000,0.6667)+1)*2*32
	# x0 = 1
	# x1 = 5
	# x2 = 10
	# x3 = 20
	# x4 = 50
	# x5 = 100

	x0 = 50
	x1 = 20
	x2 = 10
	x3 = 5
	x4 = 1
	x5 = 0.1
	labelx = 'Flow arrival rate(flows/s)'

	#x = [ x0, x1, x2, x3, x4]
	#x = [ x1, x2, x3, x4, x5]
	x, xx = algfilter(dataarray, 'short', 16, -1)
	KJoulescale = -1
	axx=   22 #55
	Energyaxy =  7#8 
	Timeaxy = 200#0.25
	location = 'lower right'#'upper left'

	numEnergy = 15
	shortEnergy, shortfullEnergy = algfilter(dataarray, 'short', numEnergy,KJoulescale)
	proEnergy, profullEnergy = algfilter(dataarray, 'priority',numEnergy,KJoulescale)
	randomEnergy, randomfullEnergy = algfilter(dataarray, 'elastic', numEnergy, KJoulescale)
	greedyEnergy, greedyfullEnergy = algfilter(dataarray, 'greedy', numEnergy, KJoulescale)


	fig = plt.figure(1,(9,4))
	plt.subplot(1,2,1)
	plt.plot(x,shortEnergy,'bv-',ms=4,  label='short')
	plt.plot(x,shortfullEnergy,'bv:', ms=4, lw=2,label='short+EXR')

	plt.plot(x,proEnergy,'kx-', label='priority')
	plt.plot(x,profullEnergy,'kx:', lw=2,label='priority+EXR')
	
	plt.plot(x,randomEnergy,'ro-', ms=4,label='random')
	plt.plot(x,randomfullEnergy,'ro:',ms=4, lw=2,label='random+EXR')
	
	plt.plot(x,greedyEnergy,'g+-', label='greedy')
	plt.plot(x,greedyfullEnergy,'g+:', lw=2,label='greedy+EXR')

	plt.legend(prop={'size':8}, loc=location)
	eetitle = '(a) Energy Efficency'
	plt.title(eetitle)
 	plt.ylabel('Network Energy Efficency(MB/J)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
 	plt.axis((0,axx,0,Energyaxy)) 
	#plt.axis((0,20,0,150)) 

	numTime = 11
	timescale = 1
	shortTime, shortfullTime = algfilter(dataarray, 'short', numTime,timescale)
	proTime, profullTime = algfilter(dataarray, 'priority',numTime, timescale)
	randomTime, randomfullTime = algfilter(dataarray, 'elastic', numTime, timescale)
	greedyTime, greedyfullTime = algfilter(dataarray, 'greedy', numTime, timescale)

	numSE = 12
	shortSE, shortfullSE = errorfilter(dataarray, 'short', numSE,timescale)
	proSE, profullSE = errorfilter(dataarray, 'priority',numSE, timescale)
	randomSE, randomfullSE = errorfilter(dataarray, 'elastic', numSE, timescale)
	greedySE, greedyfullSE = errorfilter(dataarray, 'greedy', numSE, timescale)
	plt.subplot(1,2,2)


	plt.errorbar(x,shortTime, yerr=shortSE, fmt='bv-', ms = 4, label='short')
	plt.errorbar(x,shortfullTime, yerr=shortfullSE, fmt='bv:', ms= 4, lw=2,label='short+EXR')

	plt.errorbar(x,proTime,  fmt='kx-', label='priority')
	plt.errorbar(x,profullTime, fmt='kx:', lw=2,label='priority+EXR')
	
	plt.errorbar(x,randomTime, fmt='ro-', ms= 4.0,label='random')
	plt.errorbar(x,randomfullTime, fmt='ro:', ms= 4.0, lw=2,label='random+EXR')
	
	plt.errorbar(x,greedyTime, fmt='g+-', label='greedy')
	plt.errorbar(x,greedyfullTime, fmt='g+:', lw=2,label='greedy+EXR')

	plt.legend(prop={'size':8}, loc='upper left')
	titletime = '(b) Completion Time'
	plt.title(titletime)
 	plt.ylabel('Mean flow completion time(s)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
	plt.axis((0,axx,0,Timeaxy)) 





	#plt.axis((0,20,0,150)) 

	plt.savefig('./data/bcube-ee.pdf')





	fig2 = plt.figure(2,(9,4))


	#plt.axis((0,20,0,0.8)) 

	shortEnergy, shortfullEnergy = algfilter(fattreedataarray, 'short', numEnergy,KJoulescale)
	proEnergy, profullEnergy = algfilter(fattreedataarray, 'priority',numEnergy,KJoulescale)
	randomEnergy, randomfullEnergy = algfilter(fattreedataarray, 'elastic', numEnergy, KJoulescale)
	greedyEnergy, greedyfullEnergy = algfilter(fattreedataarray, 'greedy', numEnergy, KJoulescale)

	plt.subplot(1,2,1)
	plt.plot(x,shortEnergy,'bv-', ms=4,label='short')
	plt.plot(x,shortfullEnergy,'bv:', ms=4,lw=2,label='short+EXR')

	plt.plot(x,proEnergy,'kx-', label='priority')
	plt.plot(x,profullEnergy,'kx:',lw=2, label='priority+EXR')
	
	plt.plot(x,randomEnergy,'ro-', ms=4,label='random')
	plt.plot(x,randomfullEnergy,'ro:',ms=4, lw=2,label='random+EXR')
	
	plt.plot(x,greedyEnergy,'g+-', label='greedy')
	plt.plot(x,greedyfullEnergy,'g+:', lw=2,label='greedy+EXR')


	plt.legend(prop={'size':8}, loc=location)
	plt.title(eetitle)
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
	plt.axis((0,axx,0,Energyaxy)) 
	plt.ylabel('Network Energy Efficency(MB/J)')


	shortTime, shortfullTime = algfilter(fattreedataarray, 'short', numTime,timescale)
	proTime, profullTime = algfilter(fattreedataarray, 'priority',numTime, timescale)
	randomTime, randomfullTime = algfilter(fattreedataarray, 'elastic', numTime, timescale)
	greedyTime, greedyfullTime = algfilter(fattreedataarray, 'greedy', numTime, timescale)

	shortSE, shortfullSE = errorfilter(fattreedataarray, 'short', numSE,timescale)
	proSE, profullSE = errorfilter(fattreedataarray, 'priority',numSE, timescale)
	randomSE, randomfullSE = errorfilter(fattreedataarray, 'elastic', numSE, timescale)
	greedySE, greedyfullSE = errorfilter(fattreedataarray, 'greedy', numSE, timescale)

	plt.subplot(1,2,2)


	plt.plot(x,shortTime,'bv-',  ms = 4, label='short')
	plt.plot(x,shortfullTime,'bv:',ms= 4.0, lw=2,label='short+EXR')

	plt.plot(x,proTime,'kx-', label='priority')
	plt.plot(x,profullTime,'kx:', lw=2,label='priority+EXR')
	
	plt.plot(x,randomTime,'ro-', ms=4,label='random')
	plt.plot(x,randomfullTime,'ro:', ms=4, lw=2, label='random+EXR')
	
	plt.plot(x,greedyTime,'g+-', label='greedy')
	plt.plot(x,greedyfullTime,'g+:', lw=2,label='greedy+EXR')

	plt.legend(prop={'size':8}, loc='upper left')
	plt.title(titletime)
 	plt.ylabel('Mean flow completion time(s)')
 	plt.xlabel(labelx)
 	x1,x2,y1,y2 = plt.axis()
 	plt.axis((0,axx,0,Timeaxy)) 
	#plt.axis((0,20,0,0.8)) 

	plt.savefig('./data/fattree-ee.pdf')




if __name__ == '__main__':
    sys.exit(main(sys.argv))