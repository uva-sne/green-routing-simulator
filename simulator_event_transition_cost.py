"""
Given transmission rate and size of each flows, simulate the traffic 
which includes a set of flows coming in a poisson distribution;
We consider the transmission time of devices between power on and power off, and 
study the energy consumption and completion time of Fat Tree networks
"""
import fnss
import random
import networkx as nx
import sys
import argparse
import os
import math
import csv
import numpy as np

from math import exp, sin, pi, log, sqrt
from numpy.random import lognormal, normal
from fnss.units import capacity_units, time_units

def getActivenodes(topology):
    nodeset = set()
    for edge in topology.edges(data=True):
        wgt = float(edge[2]['weight'])
        nodenum = edge[0]
        if wgt != 0 and topology.nodes(data=True)[nodenum][1]['type'] != 'host':
            nodeset.add(nodenum)
    return nodeset        


def powerCalwithCost(topology, subtopology, model, enableswbase, awakenodes):
    basepower = 0
    num_node = topology.number_of_nodes()
    dypower = 0
    nodelist = set()
    for hostnum in awakenodes:
        if topology.nodes(data=True)[hostnum][1]['type'] != 'host':
            nodelist.add(hostnum)
    print 'nodelist: '
    print nodelist
    for edge in topology.edges(data=True):
        wgt = float(edge[2]['weight'])
        nodenum = edge[0]
        if wgt != 0 and topology.nodes(data=True)[nodenum][1]['type'] != 'host':
            nodelist.add(nodenum)
            if model == 'linear':
                dypower = dypower + LinearModel(wgt)
            elif model == 'step': 
                dypower = dypower + StepModel(wgt)
            elif model == 'exponent':
                dypower = dypower + ExponentModel(wgt)
    if enableswbase == 1:
        basepower =  120*len(nodelist)
    else:
        basepower = (topology.number_of_nodes()-16)*120
                    
    power = basepower + dypower            
    return power

def powerCal(topology, subtopology, model, enableswbase):
    basepower = 0
    num_node = topology.number_of_nodes()
    dypower = 0
    nodelist = set()
    for edge in topology.edges(data=True):
        wgt = float(edge[2]['weight'])
        nodenum = edge[0]
        if wgt != 0 and topology.nodes(data=True)[nodenum][1]['type'] != 'host':
            nodelist.add(nodenum)
            if model == 'linear':
                dypower = dypower + LinearModel(wgt)
            elif model == 'step': 
                dypower = dypower + StepModel(wgt)
            elif model == 'exponent':
                dypower = dypower + ExponentModel(wgt)
    if enableswbase == 1:
        basepower =  120*len(nodelist)
    else:
        basepower = (topology.number_of_nodes()-16)*120
                    
    power = basepower + dypower            
    return power

def powerCalDiff(topology, subtopology, model, enableswbase):
    basepower = 0
    seednum = 1
    num_node = topology.number_of_nodes()
    np.random.seed(seednum)
    powerlist = np.random.normal(120,30, num_node)
    dypower = 0
    nodelist = set()
    for edge in topology.edges(data=True):
        wgt = float(edge[2]['weight'])
        nodenum = edge[0]
        if wgt != 0 and topology.nodes(data=True)[nodenum][1]['type'] != 'host':
            nodelist.add(nodenum)
            if model == 'linear':
                dypower = dypower + LinearModel(wgt)
            elif model == 'step': 
                dypower = dypower + StepModel(wgt)
            elif model == 'exponent':
                dypower = dypower + ExponentModel(wgt)
    if enableswbase == 1:
        for nodenum in nodelist:
            basepower = basepower + powerlist[nodenum]
    else:
        basepower = (topology.number_of_nodes()-16)*120
                    
    power = basepower + dypower            
    return power

def LinearModel(u):
    u= float(u)
    if (u==.0): return 0
    else: 
        return u*0.001+1

def StepModel(u):
    u= float(u)
    if (u==0.0): return 0
    elif u<=200:
        return 1+0.2
    elif u<=400 and u>200:
        return 1+0.4
    elif u<=600 and u>400:
        return 1+0.6
    elif u<=800 and u>600:  
        return 1+0.8
    elif u<=1000 and u>800: 
        return 1+1  
def ExponentModel(u):
    u= float(u)
    if (u==0.0): return 0
    else:
        #print u, math.pow(u,0.6667)
        return 0.01*math.pow(u,0.6667)+1

def postpone_Event(event_schedule, event_time, tm_flow_event):
    for time, event in event_schedule:
            if time > event_time and event['state'] == 'release':
                new_reserve_time = time + .0001
                if tm_flow_event['postponed'] == 'false':
                    tm_flow_event['postponed'] = 'true'
                    tm_flow_event['original_time'] = event_time
                print 'event_time %f is postponed to %f' %(event_time, time)
                event_schedule.add(new_reserve_time, tm_flow_event, absolute_time=True)
                break

def release_Event(event_schedule, event_time, tm_flow_event, path, full):
    if full:
        speed = 1000
    else: 
        speed = tm_flow_event['speed']
    finish_time = release_Event_Speed(event_schedule, event_time, tm_flow_event, path,speed)
    return finish_time

def release_Event_Speed(event_schedule, event_time, tm_flow_event, path, speed):      
    vol = float(tm_flow_event['vol'])
    release_time = event_time + vol/speed
    od = tm_flow_event['od_pairs']
    release_event = {'od_pairs':od , 'speed': speed, 'path': path, 'state':'release'}
    event_schedule.add(release_time, release_event, absolute_time=True) 
    if tm_flow_event['postponed'] == 'false':
        return  vol/speed
    elif tm_flow_event['postponed'] == 'true':
        return  release_time-tm_flow_event['original_time']   

def reserve_Path(topology, tmppath, flow_speed):
    i=0
    if(tmppath):
        print 'reserve target path: ' 
        print tmppath
        while (i < len(tmppath)-1):
                src = tmppath[i]
                dst = tmppath[i+1]
                edgedata = topology.get_edge_data(src,dst)
                used_wits = edgedata['weight'] + flow_speed
                i=i+1
                topology.add_weighted_edges_from([(src,dst,used_wits)]) 

def release_Path(topology, tm_flow_event, full):
    if full:
        speed = 1000
    else:    
        speed = tm_flow_event['speed']
    release_Path_Speed(topology, tm_flow_event, speed)

                  
def release_Path_Speed(topology, tm_flow_event, flow_speed):
    i=0
    tmppath = tm_flow_event['path']
    if(tmppath):
        print 'release target path: ' 
        print tmppath
        while (i < len(tmppath)-1):
                src = tmppath[i]
                dst = tmppath[i+1]
                edgedata = topology.get_edge_data(src,dst)
                used_wits = edgedata['weight'] - flow_speed
                i=i+1
                topology.add_weighted_edges_from([(src,dst,used_wits)])   

class ShortestPath(object):
    def __init__(self, event_schedule, topology, event_time, tm_flow_event):
        self.event_schedule = event_schedule
        self.topology = topology
        self.tm_flow_event = tm_flow_event
        self.time = event_time

    def getShortestPath(self, event_schedule, topology, event_time, tm_flow_event, full):
        tm_flow = tm_flow_event['od_pairs']
        if full:
            speed = 1000
        else:
            speed = tm_flow_event['speed']
        finish_time = 0.0
        power = 0
        i=0
        delay = 0
        k=0
        allpaths = []

        path_flow = tm_flow[k]
        #k=k+1
        paths2 = sorted(nx.all_shortest_paths(topology, path_flow[0], path_flow[1]), key=len)
        #paths2 = sorted(nx.all_simple_paths(topology, path_flow[0], path_flow[1]), key=len)
        #print(paths2)
        print(path_flow)
        j=0
        while(j< len(paths2)):
            tmppath = paths2[j]
            i=0
            while (i < len(tmppath)-1):
                src = tmppath[i]
                dst = tmppath[i+1]
                edgedata = topology.get_edge_data(src,dst)
                used_wits = edgedata['weight'] + speed                
                if(used_wits>1000): 
                    j=j+1
                    break
                i=i+1    
            if(i==len(tmppath)-1):   #path found
                break  
        if(j==len(paths2)):##no path with enough bandwidth
            print '**********************no enought bandwidth\n'                
            tmppath = []
            postpone_Event(event_schedule, event_time, tm_flow_event)         
        else:

            reserve_Path(topology, tmppath, speed)
            finish_time =release_Event(event_schedule, event_time ,tm_flow_event, tmppath, full)
        allpaths = tmppath + allpaths

        #subtopology = topology.subgraph(allpaths)

        #fnss.write_topology(subtopology.to_directed(), 'subtopology-short.xml')
        return allpaths, finish_time



class PriorityPath(object):
    def __init__(self, event_schedule, topology, event_time, tm_flow_event):
        self.event_schedule = event_schedule
        self.topology = topology
        self.tm_flow_event = tm_flow_event
        self.time = event_time
    def getPriorityPath(self, event_schedule, topology, event_time, tm_flow_event, full):        
        tm_flow = tm_flow_event['od_pairs']
        if full:
            speed = 1000
        else:
            speed = tm_flow_event['speed']
        finish_time = 0.0
        power = 0
        k=0
        allpaths = []
        path_flow = tm_flow[k]
        k=k+1
        paths2 = sorted(nx.all_shortest_paths(topology, path_flow[0], path_flow[1]), key=len)
        #paths2 = sorted(nx.all_simple_paths(topology, path_flow[0], path_flow[1]), key=len)
        print(path_flow)
        #print(paths2)
        pathlist = paths2[:]
        j = 0
        while (j< len(paths2)):
            tmppath = paths2[j]
            j=j+1
            i=0
            while (i < len(tmppath)-1):
                src = tmppath[i]
                dst = tmppath[i+1]
                edgedata = topology.get_edge_data(src,dst)
                used_wits = edgedata['weight'] + speed                
                if(used_wits>1000):                         
                    pathlist.remove(tmppath)
                    break
                i=i+1    
        #print("targetpathlist with enough capacity: ")
        #print(pathlist)
        if(not len(pathlist)):##if no path with enough bandwidth
            print '**********************no enought bandwidth\n'
            targetpath = []
            postpone_Event(event_schedule, event_time, tm_flow_event)
        else:
            #find active sw nodes                  
            nodeset = set()
            for edge in topology.edges(data=True):
                wgt = float(edge[2]['weight'])
                nodenum = edge[0]
                if wgt != 0 and topology.nodes(data=True)[nodenum][1]['type'] != 'host':
                    nodeset.add(nodenum)

            targetlen = len(pathlist[0]) #shortest length
            targetpathlist = []
            for targetpath in pathlist:
                if len(targetpath) > targetlen:
                    break
                targetpathlist.append(targetpath) 
            print("targetpathlist with shortest length: ")
            print(targetpathlist)
            targetcomlen=-1    
            print "active nodes: " 
            print nodeset   
            for tmppath in targetpathlist:
                comnode = set(tmppath)&nodeset
                tmpcomlen = len(comnode)
                if (tmpcomlen>targetcomlen):
                    targetcomlen=tmpcomlen
                    targetpath = tmppath
            print("targetpath: ")
            print(targetpath)  
            reserve_Path(topology, targetpath, speed)
            finish_time = release_Event(event_schedule, event_time ,tm_flow_event, targetpath, full)

        allpaths = targetpath + allpaths
        
        #subtopology = topology.subgraph(allpaths)
      

        #fnss.write_topology(subtopology, 'subtopology-pro.xml')
        return allpaths, finish_time





class RandomPath(object):
    def __init__(self, event_schedule, topology, event_time, tm_flow_event):
        self.event_schedule = event_schedule
        self.topology = topology
        self.tm_flow_event = tm_flow_event
        self.time = event_time
    def getRandomPath(self, event_schedule, topology, event_time, tm_flow_event, full):
        tm_flow = tm_flow_event['od_pairs']
        if full:
            speed = 1000
        else:
            speed = tm_flow_event['speed']
        finish_time = 0.0
        power = 0
        k=0
        allpaths = []
        path_flow = tm_flow[k]
        #k=k+1
        targetpath =[]
        paths2before = list(nx.all_simple_paths(topology, path_flow[0], path_flow[1]))
        paths2 = random.sample(paths2before, len(paths2before))
        #print(paths2)
        print(path_flow)
        j = 0
        while (j< len(paths2)):
            tmppath = paths2[j]
            j=j+1
            i=0
            while (i < len(tmppath)-1):
                src = tmppath[i]
                dst = tmppath[i+1]
                edgedata = topology.get_edge_data(src,dst)
                used_wits = edgedata['weight'] + speed                
                if(used_wits>1000):                         
                    break
                i=i+1
            if i == len(tmppath)-1:
                targetpath = tmppath
                break        
        if(not len(targetpath)):##if no path with enough bandwidth
            print '**********************no enought bandwidth\n'
            postpone_Event(event_schedule, event_time, tm_flow_event)
        else:
            print("targetpath: ")
            print(targetpath)
            reserve_Path(topology, targetpath, speed)
            finish_time =release_Event(event_schedule, event_time ,tm_flow_event, targetpath, full)

        allpaths = targetpath + allpaths
        
        #subtopology = topology.subgraph(allpaths)
      
        #fnss.write_topology(subtopology, 'subtopology-random.xml')
        return allpaths, finish_time

class GreedyPath(object):
    def __init__(self, event_schedule, topology, event_time, tm_flow_event, model, enableswbase):
        self.event_schedule = event_schedule
        self.topology = topology
        self.tm_flow_event = tm_flow_event
        self.time = event_time
        self.model = model
        self.enableswbase = enableswbase
    def greenPathLookup(self, allpaths, pathlist,  model, enableswbase, speed):
        targetpower = 10**100           
        for targetpath in pathlist: 
            tmptopology = fnss.read_topology('tmptopology-greedy.xml')  
            #tmptopology = self.topology[:]
            i=0
            while (i < len(targetpath)-1):
                src = targetpath[i]
                dst = targetpath[i+1]
                edgedata = tmptopology.get_edge_data(src,dst)
                used_wits = float(edgedata['weight']) + speed
                i=i+1 
                #print src, dst, edgedata['weight'], used_wits                   
                tmptopology.add_weighted_edges_from([(src,dst,used_wits)]) 
            alltargetpaths = allpaths + targetpath
            tmpsubtoplogy = tmptopology.subgraph(alltargetpaths)
            power = powerCal(tmptopology, tmpsubtoplogy, model, enableswbase)
            #print targetpath, power
            if power < targetpower:
                targetpower = power
                greenpath = targetpath
        return greenpath 
            

    def getGreedyPath(self, event_schedule, topology, event_time, tm_flow_event, full, model, enableswbase):
        tm_flow = tm_flow_event['od_pairs']
        if full:
            speed = 1000
        else:
            speed = tm_flow_event['speed']
        finish_time = 0.0
        power = 0
        k=0
        allpaths = []
        path_flow = tm_flow[k]
        k=k+1
        paths2 = sorted(nx.all_simple_paths(topology, path_flow[0], path_flow[1]), key=len)
        #print(paths2)
        print(path_flow)
        pathlist = paths2[:]

        for tmppath in paths2:#path with enough capacity
            i=0
            while (i < len(tmppath)-1):
                src = tmppath[i]
                dst = tmppath[i+1]
                edgedata = topology.get_edge_data(src,dst)
                used_wits = edgedata['weight'] + speed                
                if(used_wits>1000): 
                    pathlist.remove(tmppath)
                    break
                i=i+1    
        print("targetpathlist with enough capacity: ")
        #print(pathlist)
        if(not len(pathlist)):##if no path with enough bandwidth
            print '**********************no enought bandwidth\n'                
            targetpath = []
            postpone_Event(event_schedule, event_time, tm_flow_event)         
        else:
            nodeset = set()
            for edge in topology.edges(data=True):
                wgt = float(edge[2]['weight'])
                nodenum = edge[0]
                if wgt != 0 and topology.nodes(data=True)[nodenum][1]['type'] != 'host':
                    nodeset.add(nodenum)
            print 'active nodes: '
            print nodeset
            fnss.write_topology(topology, 'tmptopology-greedy.xml')  
            targetpath = self.greenPathLookup(allpaths, pathlist,  model, enableswbase, speed)
            print targetpath
            reserve_Path(topology, targetpath, speed)
            finish_time =release_Event(event_schedule, event_time ,tm_flow_event, targetpath, full)
 

        allpaths = targetpath + allpaths
    

        #fnss.write_topology(subtopology, 'subtopology-greedy.xml')
        return allpaths, finish_time   



def usage():
    """
    Prints usage.
    """
    print ' [-p algorithm] [-m model][-b][-h]'
    print ' -p routing algorithm: short, priority, greedy, elastic.'
    print ' -m power model: step, linear, exponent.'
    print ' -b : only power off links and always enable switches'
    print ' -f : file name of traffic matrix'
    print ' -h :     Show help (what you are reading right now).'
    print ' -u utilization of links :    float'
    print ' -n -l number of flows: n*l   int '
    print ' -r average of flow arrival interval:   float '
    print ' -e: exclusively use the full link bandwidth '
    print ' -c cost of transition time: float '

def getoptions(argv):
    """
    Parses the command line options.
    """
    import getopt
    try:
        opts, args = getopt.getopt(argv, 'hp:m:f:u:n:l:b:r:c:e')
    except getopt.GetoptError, err:
        print err
        usage()
        sys.exit(2)
    alg    = 'short'
    model = 'linear'
    enableswbase = 1
    filename = ''
    max_u = 0.8
    n = 3
    l = 3
    r= 100
    full = False
    c = 1.0

    for o, a in opts:
        if o == '-h':
            usage()
            sys.exit(1)
        elif o == '-p':
            alg = a
        elif o == '-m':
             model = a     
        elif o == '-b':
            enableswbase = 0
        elif o == '-f':
            filename = a
        elif o == '-u':
            max_u = float(a)
        elif o == '-n':
            n = int(a)
        elif o == '-l':
            l = int(a)
        elif o == '-r':
            r = float(a)
        elif o == '-e': 
            full = True      
        elif o == '-c': 
            c = float(a)   
    return alg, model, enableswbase, filename , max_u, n, l, r, full, c

def rand_flow():
    #edgenodes = xrange(2**4)      #bcube
    edgenodes = range(20,36)    #fattree
    onodes = random.sample(edgenodes, 1)
    dlist = [x for x in edgenodes if x not in onodes]
    dnodes = random.sample(dlist, 1)
    od_pairs = [(o, d) for o in onodes for d in dnodes if o != d]
    throughputs, volumns = rand_traffic(500., 10. , 5000., 10., 1000)
    speed = int(random.choice(throughputs))
    vol = random.choice(volumns)
    return {'od_pairs': od_pairs, 'speed': speed, 'vol': vol, 'state': 'reserve','postponed': 'false'}

def rand_traffic(mean, stddev, vol_mean, vol_stddev, num_events):
    mu = log(mean**2/sqrt(stddev**2 + mean**2))
    sigma = sqrt(log((stddev**2/mean**2) + 1))  
    throughputs = lognormal(mu, sigma, size=num_events)  

    vol_mu = log(vol_mean**2/sqrt(vol_stddev**2 + vol_mean**2))
    vol_sigma = sqrt(log((vol_stddev**2/vol_mean**2) + 1))  
    volumns = lognormal(vol_mu, vol_sigma, size=num_events) 
    return throughputs, volumns 


def main(args):
    alg, model, enableswbase,filename,u,n, l, r, full,costTime  = getoptions(args)

    totalenergy = 0
    totalenergylist = []
    repeats = 1
    total_time = 0.0
    total_timelist = []
    total_power = 0.0
    total_powerlist = []
    eelist = []

    for repeat in xrange(repeats): 
        netenergy = 0
        finish_times = 0.0 
        topology = fnss.fat_tree_topology(4)
        #topology = fnss.bcube_topology(2,3)
        fnss.set_weights_constant(topology, 1)
        fnss.set_delays_constant(topology, 1, 'ms')
        topology = topology.copy() if topology.is_directed() \
            else topology.to_directed()

        fnss.set_capacities_constant(topology, 1000, 'Mbps')

        subtopo = fnss.Topology()
        fnss.set_weights_constant(topology, 0)
        fnss.write_topology(topology, './data/topology.xml')
        random.seed(repeat)
        np.random.seed(repeat)
        event_schedule = fnss.poisson_process_event_schedule(
                        avg_interval=float(r),               
                        t_start=0,                      # starts at 0
                        duration= 60,                   # 1 hours
                        t_unit='s',                   # seconds
                        event_generator= rand_flow,  # event gen function
                        
                        )
        num_events = event_schedule.number_of_events()
        size =0
        total_size = 0
        for arrival_time, tm_flow in event_schedule:
            size = tm_flow['vol']
            total_size = total_size + size

        fnss.write_event_schedule(event_schedule, './data/events.xml')
        repeat_event_schedule = fnss.read_event_schedule('./data/events.xml')

        reserve_event_schedule = {}

        for arrival_time, tm_flow in event_schedule:
            if tm_flow['state'] == 'reserve':
                if alg =='short':
                    shortpath = ShortestPath(event_schedule, topology, arrival_time, tm_flow)
                    subtopo, finish_time = shortpath.getShortestPath(event_schedule, topology, \
                        arrival_time, tm_flow, full)
                elif alg == 'priority':
                    propath = PriorityPath(event_schedule, topology, arrival_time, tm_flow)
                    subtopo, finish_time = propath.getPriorityPath(event_schedule, topology, \
                        arrival_time, tm_flow, full)
                elif alg == 'greedy':
                    greedypath = GreedyPath(event_schedule, topology, arrival_time, tm_flow, model, enableswbase)   
                    subtopo, finish_time = greedypath.getGreedyPath(event_schedule, topology, \
                        arrival_time, tm_flow, full, model, enableswbase) 
                elif alg == 'elastic':
                    randompath = RandomPath(event_schedule, topology, arrival_time, tm_flow)   
                    subtopo, finish_time = randompath.getRandomPath(event_schedule, topology, \
                        arrival_time, tm_flow, full) 
                if len(subtopo):    
                    reserve_event_schedule[str(arrival_time)] = subtopo
            elif tm_flow['state'] == 'release':
                release_Path(topology, tm_flow, full)





        it = 0
        last_arrival_time = 0.0
        last_power = 0.0
        finish_time = 0
        events_power = 0.0
         
        for arrival_time, tm_flow in repeat_event_schedule:
            print alg + str(repeat), it
            if tm_flow['state'] == 'reserve':
                keepawakenodes = set()
                for reserve_arrivale_time, reserve_path in reserve_event_schedule.iteritems():
                    if 0 < float(reserve_arrivale_time) - arrival_time < costTime:
                        for nodeid in reserve_path:
                            keepawakenodes.add(nodeid)
                if alg =='short':
                    shortpath = ShortestPath(repeat_event_schedule, topology, arrival_time, tm_flow)
                    subtopo, finish_time = shortpath.getShortestPath(repeat_event_schedule, topology, \
                        arrival_time, tm_flow, full)
                elif alg == 'priority':
                    propath = PriorityPath(repeat_event_schedule, topology, arrival_time, tm_flow)
                    subtopo, finish_time = propath.getPriorityPath(repeat_event_schedule, topology, \
                        arrival_time, tm_flow, full)
                elif alg == 'greedy':
                    greedypath = GreedyPath(repeat_event_schedule, topology, arrival_time, tm_flow, model, enableswbase)   
                    subtopo, finish_time = greedypath.getGreedyPath(repeat_event_schedule, topology, \
                        arrival_time, tm_flow, full, model, enableswbase) 
                elif alg == 'elastic':
                    randompath = RandomPath(repeat_event_schedule, topology, arrival_time, tm_flow)   
                    subtopo, finish_time = randompath.getRandomPath(repeat_event_schedule, topology, \
                        arrival_time, tm_flow, full)  




            elif tm_flow['state'] == 'release':
                finish_time = 0.0
                release_Path(topology, tm_flow, full)
            interval = arrival_time - last_arrival_time
            energy = last_power*interval
            print tm_flow, interval, last_power, finish_time
            power = powerCalwithCost(topology, subtopo, model, enableswbase, keepawakenodes)
            last_power = power
            last_arrival_time = repeat_event_schedule[it][0]
            it+=1
            netenergy = netenergy + energy
            finish_times = finish_times + finish_time
            print power, num_events, netenergy
            
        totalenergylist.append(netenergy)
        totalenergy = totalenergy + netenergy
        avg_time = finish_times/num_events
        total_timelist.append(avg_time)
        avg_power = netenergy/60
        total_powerlist.append(avg_power)
        eelist.append(netenergy/total_size)
    energyarray =  np.array(totalenergylist)
    timearray = np.array(total_timelist)
    energyarraystd = np.std(energyarray)
    energyarraymean = np.mean(energyarray)
    powerarray = np.array(total_powerlist)
    eearray = np.array(eelist)

    row = [alg, model, enableswbase, u, filename, energyarraymean, 1.0 , n, l, energyarraystd, \
        full, np.mean(timearray),np.std(timearray), np.mean(powerarray),np.std(powerarray), \
        np.mean(eearray),  r,costTime]
    # for arrival_time, tm_flow in event_schedule:
    #     print arrival_time, tm_flow
    print energyarray, timearray
    with open('./data/routing-event-cost.csv', "a") as fp:
        fdata = csv.writer(fp, delimiter=',')
        #fdata.writerow(head)
        fdata.writerow(row)


if __name__ == '__main__':
    main(sys.argv[1:])
