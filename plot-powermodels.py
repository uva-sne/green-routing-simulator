# plot the curves of power model


import csv
import numpy as np
import sys
import matplotlib.pyplot as plt
import math

def LinearModel(u):
    return u*0.001+1

def StepModel(u):
    if u<=200:
        return 1+0.2
    elif u<=400 and u>200:
    	return 1+0.4
    elif u<=600 and u>400:
    	return 1+0.6
    elif u<=800 and u>600:	
    	return 1+0.8
    elif u<=1000 and u>800:	
    	return 1+1	
def ExponentModel(u):
    return 0.01*math.pow(u,0.6667)+1

def main(argv):
	
	x = [0,200.0,201.0,400.0,401.0,600.0,601.0,800.0,801.0,1000.0]
	lineary=[]
	stepy=[]
	exponenty=[]
	for x1 in x:
		y1=LinearModel(x1)
		lineary.append(y1)
		y2=StepModel(x1)
		stepy.append(y2)
		y3=ExponentModel(x1)
		exponenty.append(y3)

	linear = plt.plot(x,lineary,'b', label='linear')


	step = plt.plot(x,stepy,'r', label='step')

	exponent = plt.plot(x,exponenty,'y', label='exponent')

	
	plt.legend(prop={'size':10}, loc='upper left')
	#ax1,ax2,ay1,ay2 = plt.axis()
	#plt.axis((0,1,0,1)) 
	plt.title(' Different power models of a link')
	plt.ylabel('link power')
	plt.xlabel('link utilization')
	plt.savefig("data/powermodel.eps")


if __name__ == '__main__':
    sys.exit(main(sys.argv))